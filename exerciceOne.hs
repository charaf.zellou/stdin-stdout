listNumbers a b c d e f = [a, b, c, d, e, f]

getTopThree thisList = take 3 thisList

numberIsEven x = (mod x 2)

resultList = map numberIsEven (getTopThree (listNumbers 1 2 3 4 5 6))

